[![Testspace](https://www.testspace.com/img/Testspace.png)](https://www.testspace.com)

***

## Getting Started Examples 

Sample demonstrates techniques for using Testspace:

  * Using a Testspace Project that is `connected` with this Bitbucket Repo
  * Using Bitbucket Pipeline for demonstration purposes only
  * Refer to our [Getting Started](https://help.testspace.com/getting.started) help articles for more information

***

Bitbucket Pipelines Example

For adding Testspace to your .bitbucket-pipelines.yml refer to [add-to-ci](https://help.testspace.com/publish/push-data-add-to-ci#bitbucket-pipelines)

`Published content` at https://samples.testspace.com/projects/testspace-samples:getting.started-bitbucket.

 [![Space Health](https://samples.testspace.com/spaces/74117/badge?token=08cfc06e199878355b00836cc70690e0ef1f574a)](https://samples.testspace.com/spaces/74117 "Test Cases")
 [![Space Metric](https://samples.testspace.com/spaces/74117/metrics/36763/badge?token=8318cd7a8cc63df2b413ed68520ee6bfb9699642)](https://samples.testspace.com/spaces/74117/schema/Code%20Coverage "Code Coverage (lines)")
 [![Space Metric](https://samples.testspace.com/spaces/74117/metrics/36765/badge?token=9520ceea9114dfa83eea8fe85e029a51f8c2e961)](https://samples.testspace.com/spaces/74117/schema/Static%20Analysis "Static Analysis (issues)")


Download and configure the *Testspace client*: 


   `curl -fsSL https://testspace-client.s3.amazonaws.com/testspace-linux.tgz | tar -zxvf- -C /usr/local/bin`
   
   `testspace config url YOUR-subdomain.testspace.com`


Note that an *access-token* is NOT required when configuring a public repo. 

Push content using *Testspace client*:

   `testspace analysis.xml [tests]results*.xml coverage.xml` 
 

